# india/fintech

---

## Agenda

- Some demos
- How money moves around?

---

## Ways to move money

- NEFT/RTGS/IMPS
- UPI
- Wallets

---

## How money moves

```sql
UPDATE balance set current=current-amount WHERE id=source
UPDATE balance set current=current+amount WHERE id=destination
```

---

## How money really moves

![](rube.jpg)

- [Source](https://krypt3ia.wordpress.com/2011/11/15/infosec-the-worlds-largest-rube-goldberg-device/)

---

# CBS (Core Banking System)

---

# payments v/s transfers

## merchant
## payer
## payee

---

# payments v/s transfers

## merchant

business that takes your money

## payer

the person paying

## payee

someone else getting your money

---

# P2M / P2P

- Payer to Merchant
- Payer to Payee

---

# wallets

(private ledger / PPI)

---

# DEMO - Paytm (P2M)

---

# pre-demo

- $1 = &#8377; 73
- Payer = NEMO
- Merchant = Paytm
- Payment Method = *

---

# DEMO - UPI